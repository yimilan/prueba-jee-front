import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private clienteApiUrl = `${environment.apiDomain}/clientes`;
  private ventasApiUrl = `${environment.apiDomain}/ventas`;
  constructor(private http: HttpClient) { }

  public obtenerClientes(): Observable<any> {
    return this.http.get<any>(this.clienteApiUrl);
  }

  public obtenerClientePorUuid(uuid: string): Observable<any> {
    return this.http.get<any>(`${this.clienteApiUrl}/buscar-cliente`, { params: { uuid } });
  }
  public obtenerClientePorEmail(email: string): Observable<any> {
    return this.http.get<any>(`${this.clienteApiUrl}/buscar-cliente`, { params: { email } });
  }

  public obtenerHistorialRecargasDeClientePorUuid(clienteUuid: string): Observable<any> {
    return this.http.get<any>(`${this.ventasApiUrl}/buscar-venta-cliente`, { params: { clienteUuid } });
  }


  registrarCliente(clienteData: { name: string; document: number; email: string }): Observable<any> {
    return this.http.post(this.clienteApiUrl, clienteData, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
}
