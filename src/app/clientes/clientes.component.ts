import { Component, OnInit } from '@angular/core';
import { ClienteService } from './cliente.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  clientes: any = [];
  clienteSeleccionado: any;

  constructor(private clienteService: ClienteService, private router: Router) { }

  ngOnInit() {
    this.cargarClientes();
  }

  cargarClientes() {
    this.clienteService.obtenerClientes().subscribe({
      next: (data) => this.clientes = data,
      error: (error) => console.error('Error al obtener clientes', error)
    });
  }

  obtenerClientePorUuid(uuid: string) {
    this.clienteService.obtenerClientePorUuid(uuid).subscribe({
      next: (cliente) => this.clienteSeleccionado = cliente,
      error: (error) => console.error('Error al obtener cliente', error)
    });
  }

  obtenerHistorialRecargasDeClientePorUuid(cliente: any) {
    this.router.navigateByUrl(`/clientes/${cliente.uuid}/historial-recargas`);
  }

  irACrearCliente() {
    this.router.navigate(['/clientes/registrar-cliente']); // Asegúrate de que la ruta esté definida en tu módulo de rutas
  }
}