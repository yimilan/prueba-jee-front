import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../clientes/cliente.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { VentaService } from '../ventas/venta.service';

@Component({
  selector: 'app-historial-recargas',
  templateUrl: './historial-recargas.component.html',
  styleUrls: ['./historial-recargas.component.css']
})
export class HistorialRecargasComponent implements OnInit {
  clienteUuid!: string;
  historial: any;
  cliente: any;
  enRutaDeHistorial = true;
  recargaSeleccionada: any;
  estadoSeleccionado!: string;
  mostrarAlerta = false;
  mensajeAlerta = '';

  constructor(private clienteService: ClienteService, private ventaService: VentaService, private activateRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      this.clienteUuid = params['clienteUuid'];
      this.cargarDetallesCliente(this.clienteUuid);
      this.obtenerHistorialRecargasDeClientePorUuid(this.clienteUuid);
    });
  }

  cargarDetallesCliente(clienteUuid: string) {
    this.clienteService.obtenerClientePorUuid(clienteUuid).subscribe({
      next: (cliente) => {
        this.cliente = cliente; // Asegúrate de que la propiedad 'cliente' esté definida en tu clase
        // Puedes llamar aquí a otro método para cargar el historial de recargas si es necesario
      },
      error: (error) => console.error('Error al obtener cliente', error)
    });
  }

  obtenerVentaPorUuid(uuid: string) {
    this.ventaService.obtenerVentaPorUuid(uuid).subscribe({
      next: (data) => this.recargaSeleccionada = data,
      error: (error) => console.error('Error al obtener cliente', error)
    });
  }

  obtenerHistorialRecargasDeClientePorUuid(clienteUuid: string) {
    this.clienteService.obtenerHistorialRecargasDeClientePorUuid(clienteUuid).subscribe({
      next: (data) => this.historial = data, // Modificado para almacenar los detalles del cliente
      error: (error) => console.error('Error al obtener cliente', error)
    });
  }

  cambiarEstadoDeVenta(clienteUuid: string, estado: string) {
  
    this.ventaService.cambiarEstadoVenta(clienteUuid, estado).subscribe({
      next: (response) => {
        this.mostrarMensaje('Estado de la venta cambiado con éxito.', 'success');
      },
      error: (error) => {
        this.mostrarMensaje('Error al cambiar el estado de la venta.', 'danger');
      }
    });
  }

  mostrarMensaje(mensaje: string, tipo: string) {
    this.mensajeAlerta = mensaje;
    this.mostrarAlerta = true;

    // Opcional: Ocultar la alerta automáticamente después de unos segundos
    setTimeout(() => {
      this.mostrarAlerta = false;
    }, 3000);
  }

  volverAlListado() {
    this.enRutaDeHistorial = false;
    this.router.navigateByUrl(`/clientes`, { replaceUrl: false });
  }
  irACrearVenta() {
    this.router.navigate(['/ventas/crear-venta']); // Asegúrate de que la ruta esté definida en tu módulo de rutas
  }
}
