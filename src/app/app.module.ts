import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientesComponent } from './clientes/clientes.component';
import { HistorialRecargasComponent } from './historial-recargas/historial-recargas.component';
import { FormsModule } from '@angular/forms';
import { RegistrarClienteComponent } from './registrar-cliente/registrar-cliente.component';
import { OperadoresComponent } from './operadores/operadores.component';
import { VendedoresComponent } from './vendedores/vendedores.component';
import { VentasComponent } from './ventas/ventas.component';
import { CrearVentasComponent } from './crear-ventas/crear-ventas.component';
import { ClientAccessComponent } from './client-access/client-access.component';

@NgModule({
  declarations: [
    AppComponent,
    ClientesComponent,
    HistorialRecargasComponent,
    RegistrarClienteComponent,
    OperadoresComponent,
    VendedoresComponent,
    VentasComponent,
    CrearVentasComponent,
    ClientAccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
