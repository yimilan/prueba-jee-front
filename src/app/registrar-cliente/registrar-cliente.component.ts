export interface Cliente {
  email: string;
  name: string;
  document: number;
}

import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { ClienteService } from '../clientes/cliente.service';

@Component({
  selector: 'app-registrar-cliente',
  templateUrl: './registrar-cliente.component.html',
  styleUrls: ['./registrar-cliente.component.css']
})
export class RegistrarClienteComponent {
  clienteData: Cliente = {
    email: '',
    name: '',
    document: 0
  };
  mostrarAlerta = false;
  mensajeAlerta = '';

  constructor(private clienteService: ClienteService, private router: Router) {}

  registrarCliente(clienteData: Cliente) {
    this.clienteService.registrarCliente(clienteData).subscribe({
      next: (response) => this.mostrarMensaje('Cliente registrado con éxito.', 'success'),
      error: (error) => this.mostrarMensaje('Error al registrar el cliente.', 'danger')
    });
  }

  mostrarMensaje(mensaje: string, tipo: string) {
    this.mensajeAlerta = mensaje;
    this.mostrarAlerta = true;

    // Opcional: Ocultar la alerta automáticamente después de unos segundos
    setTimeout(() => {
      this.mostrarAlerta = false;
    }, 3000);
  }

  volverAlListado() {
    this.router.navigateByUrl(`/clientes`, { replaceUrl: false });
  }
}
