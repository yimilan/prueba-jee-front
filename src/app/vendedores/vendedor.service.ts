import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VendedorService {
  private vendedorApiUrl = `${environment.apiDomain}/vendedores`;
  constructor(private http: HttpClient) { }

  public obtenerVendedores(): Observable<any> {
    return this.http.get<any>(this.vendedorApiUrl);
  }

  public obtenerVendedorPorUuid(uuid: string): Observable<any> {
    return this.http.get<any>(this.vendedorApiUrl, { params: { uuid } });
  }

  registrarVendedor(vendedorData: { name: string; document: number, telephone: string,  email: string}): Observable<any> {
    return this.http.post(this.vendedorApiUrl, vendedorData, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
}
