export interface Vendedor {
  name: string;
  document: number;
  telephone: string;
  email: string;
}

import { Component } from '@angular/core';
import { VendedorService } from './vendedor.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.component.html',
  styleUrls: ['./vendedores.component.css']
})
export class VendedoresComponent {
  vendedorData: Vendedor = {
    name: '',
    document: 0,
    telephone: '',
    email: ''
  };
  mostrarAlerta = false;
  mensajeAlerta = '';
  vendedores: any = [];
  vendedorSeleccionado: any;
  constructor(private vendedorService: VendedorService, private router: Router) { }

  ngOnInit() {
    this.cargarVendedores();
  }

  cargarVendedores() {
    this.vendedorService.obtenerVendedores().subscribe({
      next: (data) => this.vendedores = data,
      error: (error) => console.error('Error al obtener vendedors', error)
    });
  }

  registrarVendedor(vendedorData: Vendedor) {
    this.vendedorService.registrarVendedor(vendedorData).subscribe({
      next: (response) => this.mostrarMensaje('Vendedor registrado con éxito.', 'success'),
      error: (error) => this.mostrarMensaje('Error al registrar el vendedor.', 'danger')
    });
  }

  mostrarMensaje(mensaje: string, tipo: string) {
    this.mensajeAlerta = mensaje;
    this.mostrarAlerta = true;

    // Opcional: Ocultar la alerta automáticamente después de unos segundos
    setTimeout(() => {
      this.mostrarAlerta = false;
    }, 3000);
  }
}
