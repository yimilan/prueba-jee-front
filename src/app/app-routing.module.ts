import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientesComponent } from './clientes/clientes.component';
import { HistorialRecargasComponent } from './historial-recargas/historial-recargas.component';
import { RegistrarClienteComponent } from './registrar-cliente/registrar-cliente.component';
import { OperadoresComponent } from './operadores/operadores.component';
import { VendedoresComponent } from './vendedores/vendedores.component';
import { VentasComponent } from './ventas/ventas.component';
import { CrearVentasComponent } from './crear-ventas/crear-ventas.component';
import { ClientAccessComponent } from './client-access/client-access.component';

const routes: Routes = [
  { path: 'clientes', component: ClientesComponent},
  { path: 'clientes/acceso', component: ClientAccessComponent},
  { path: 'clientes/:clienteUuid/historial-recargas', component: HistorialRecargasComponent},
  { path: 'clientes/registrar-cliente', component: RegistrarClienteComponent},
  { path: 'operadores', component: OperadoresComponent},
  { path: 'vendedores', component: VendedoresComponent},
  { path: 'ventas', component: VentasComponent},
  { path: 'ventas/crear-venta', component: CrearVentasComponent},
  // otras rutas...
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
