import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OperadorService {
  private operadorApiUrl = `${environment.apiDomain}/operadores`;
  constructor(private http: HttpClient) { }

  public obtenerOperadores(): Observable<any> {
    return this.http.get<any>(this.operadorApiUrl);
  }

  public obtenerOperadorPorUuid(uuid: string): Observable<any> {
    return this.http.get<any>(this.operadorApiUrl, { params: { uuid } });
  }

  registrarOperador(operadorData: { name: string; rut: string }): Observable<any> {
    return this.http.post(this.operadorApiUrl, operadorData, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
}
