export interface Operador {
  name: string;
  rut: string;
}

import { Component, OnInit } from '@angular/core';
import { OperadorService } from './operador.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-operadores',
  templateUrl: './operadores.component.html',
  styleUrls: ['./operadores.component.css']
})
export class OperadoresComponent {
  operadorData: Operador = {
    name: '',
    rut: ''
  };
  mostrarAlerta = false;
  mensajeAlerta = '';
  operadores: any = [];
  operadorSeleccionado: any;
  constructor(private operadorService: OperadorService, private router: Router) { }

  ngOnInit() {
    this.cargarOperadores();
  }

  cargarOperadores() {
    this.operadorService.obtenerOperadores().subscribe({
      next: (data) => this.operadores = data,
      error: (error) => console.error('Error al obtener operadors', error)
    });
  }

  registrarOperador(operadorData: Operador) {
    this.operadorService.registrarOperador(operadorData).subscribe({
      next: (response) => this.mostrarMensaje('Operador registrado con éxito.', 'success'),
      error: (error) => this.mostrarMensaje('Error al registrar el operador.', 'danger')
    });
  }

  mostrarMensaje(mensaje: string, tipo: string) {
    this.mensajeAlerta = mensaje;
    this.mostrarAlerta = true;

    // Opcional: Ocultar la alerta automáticamente después de unos segundos
    setTimeout(() => {
      this.mostrarAlerta = false;
    }, 3000);
  }
}
