import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientAccessComponent } from './client-access.component';

describe('ClientAccessComponent', () => {
  let component: ClientAccessComponent;
  let fixture: ComponentFixture<ClientAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientAccessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClientAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
