// En ClientAccessComponent

import { Component } from '@angular/core';
import { ClienteService } from '../clientes/cliente.service'; // Asegúrate de actualizar esta ruta
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-access',
  templateUrl: './client-access.component.html',
  styleUrls: ['./client-access.component.css']
})
export class ClientAccessComponent {
  email: string = '';
  mostrarAlerta = false;
  mensajeAlerta = '';

  constructor(private clienteService: ClienteService, private router: Router) {}

  accederConEmail() {
    this.clienteService.obtenerClientePorEmail(this.email).subscribe({
      next: (cliente) => this.router.navigateByUrl(`/clientes/${cliente.uuid}/historial-recargas`),
      error: (error) => this.mostrarMensaje('No existe el cliente.', 'danger')
    });
  }

  mostrarMensaje(mensaje: string, tipo: string) {
    this.mensajeAlerta = mensaje;
    this.mostrarAlerta = true;

    // Opcional: Ocultar la alerta automáticamente después de unos segundos
    setTimeout(() => {
      this.mostrarAlerta = false;
    }, 3000);
  }
}
