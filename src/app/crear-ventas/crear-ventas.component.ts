import { Component } from '@angular/core';
import { VentaService } from '../ventas/venta.service'; // Asegúrate de importar VentaService
import { OperadorService } from '../operadores/operador.service';
import { VendedorService } from '../vendedores/vendedor.service';
import { ClienteService } from '../clientes/cliente.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-ventas',
  templateUrl: './crear-ventas.component.html',
  styleUrls: ['./crear-ventas.component.css']
})
export class CrearVentasComponent {
  nuevaVenta = {
    datatimeSale: '',
    total: 0,
    recharged_phone: '',
    status: '',
    client_uuid: '',
    operator_uuid: '',
    seller_uuid: ''
  };
  vendedores: any = [];
  operadores: any = [];
  clientes: any = [];
  mostrarAlerta = false;
  mensajeAlerta = '';

  constructor(private operadorService: OperadorService, private vendedorService: VendedorService,private ventaService: VentaService, private clienteService: ClienteService, private router: Router) {}

  ngOnInit() {
    this.cargarVendedores();
    this.cargarOperadores();
    this.cargarClientes();
  }
  cargarClientes() {
    this.clienteService.obtenerClientes().subscribe({
      next: (data) => this.clientes = data,
      error: (error) => console.error('Error al obtener clientes', error)
    });
  }

  cargarVendedores() {
    this.vendedorService.obtenerVendedores().subscribe({
      next: (data) => this.vendedores = data,
      error: (error) => console.error('Error al obtener vendedors', error)
    });
  }
  cargarOperadores() {
    this.operadorService.obtenerOperadores().subscribe({
      next: (data) => this.operadores = data,
      error: (error) => console.error('Error al obtener operadors', error)
    });
  }
  registrarNuevaVenta() {
    this.ventaService.registrarVenta(this.nuevaVenta).subscribe({
      next: (response) => {
        this.mostrarMensaje('Venta registrada con éxito.', 'success')
      },
      error: (error) => {
        console.error('Error al registrar la venta', error);
        this.mostrarMensaje('Error al registrar la venta.', 'danger')
      }
    });
  }

  volverAlListado() {
    this.router.navigateByUrl(`/ventas`, { replaceUrl: false });
  }

  mostrarMensaje(mensaje: string, tipo: string) {
    this.mensajeAlerta = mensaje;
    this.mostrarAlerta = true;

    // Opcional: Ocultar la alerta automáticamente después de unos segundos
    setTimeout(() => {
      this.mostrarAlerta = false;
    }, 3000);
  }
}
