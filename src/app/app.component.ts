import { Component } from '@angular/core';
import { Router, NavigationEnd, Event as RouterEvent } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  enRutaInicio: boolean = false;
  title = 'frontend-prueba-JEE';

  constructor(private router: Router) {
    // Escuchar cambios en la ruta
    this.router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      map(event => event as NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      this.enRutaInicio = event.url === '/';
    });
  }

  esInicio(): boolean {
    return this.enRutaInicio;
  }
}
