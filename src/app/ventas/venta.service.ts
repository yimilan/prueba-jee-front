import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VentaService {
  private ventasApiUrl = `${environment.apiDomain}/ventas`;
  constructor(private http: HttpClient) { }

  public obtenerVentas(): Observable<any> {
    return this.http.get<any>(this.ventasApiUrl);
  }

  public obtenerVentaPorUuid(uuid: string): Observable<any> {
    return this.http.get<any>(`${this.ventasApiUrl}/buscar-venta`, { params: { uuid } });
  }

  public cambiarEstadoVenta(ventaUuid: string, nuevoEstado: string): Observable<any> {
    const params = { ventaUuid, nuevoEstado };
    return this.http.post<any>(`${this.ventasApiUrl}/cambiar-estado`, null, { params });
  }

  public buscarVentaPorVendedor(vendedorUuid: string): Observable<any> {
    const params = new HttpParams().set('vendedorUuid', vendedorUuid);
    return this.http.get<any>(`${this.ventasApiUrl}/buscar-venta-vendedor`, { params });
  }

  public buscarVentaPorOperador(operadorUuid: string): Observable<any> {
    const params = new HttpParams().set('operadorUuid', operadorUuid);
    return this.http.get<any>(`${this.ventasApiUrl}/buscar-venta-operador`, { params });
  }

  public buscarVentaPorVendedorYOperador(vendedorUuid: string, operadorUuid: string): Observable<any> {
    const params = new HttpParams().set('vendedorUuid', vendedorUuid).set('operadorUuid', operadorUuid);
    return this.http.get<any>(`${this.ventasApiUrl}/buscar-venta-vendedor-operador`, { params });
  }

  public registrarVenta(ventaData: {
    datatimeSale: string;
    total: number;
    recharged_phone: string;
    status: string;
    client_uuid: string;
    operator_uuid: string;
    seller_uuid: string;
  }): Observable<any> {
    return this.http.post<any>(this.ventasApiUrl, ventaData, {
      headers: { 'Content-Type': 'application/json' }
    });
  }
}
