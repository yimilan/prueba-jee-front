export interface Venta {
  datatimeSale: string;
  total: number;
  recharged_phone: string;
  status: string,
  client_uuid: string,
  operator_uuid: string,
  seller_uuid: string
}

import { Component, OnInit } from '@angular/core';
import { VentaService } from './venta.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { VendedorService } from '../vendedores/vendedor.service';
import { OperadorService } from '../operadores/operador.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent {
  ventaData: Venta = {
    datatimeSale: '',
    total: 0,
    recharged_phone: '',
    status: '',
    client_uuid: '',
    operator_uuid: '',
    seller_uuid: ''
  };
  ventas: any = [];
  vendedores: any = [];
  operadores: any = [];
  ventaSeleccionado: any;
  recargaSeleccionada: any;
  estadoSeleccionado!: string;
  mostrarAlerta = false;
  mensajeAlerta = '';
  
  filtroOperadorUuid: string = '';
  filtroVendedorUuid: string = '';
  totalVentas: number = 0;
  cantidadVentas: number = 0;

  constructor(private operadorService: OperadorService, private vendedorService: VendedorService, private ventaService: VentaService, private router: Router) { }

  ngOnInit() {
    this.cargarVendedores();
    this.cargarOperadores();
    this.cargarVentas();
  }

  cargarVendedores() {
    this.vendedorService.obtenerVendedores().subscribe({
      next: (data) => this.vendedores = data,
      error: (error) => console.error('Error al obtener vendedors', error)
    });
  }
  cargarOperadores() {
    this.operadorService.obtenerOperadores().subscribe({
      next: (data) => this.operadores = data,
      error: (error) => console.error('Error al obtener operadors', error)
    });
  }

  cargarVentas() {
    let observableVentas;
    if (this.filtroOperadorUuid && this.filtroVendedorUuid) {
      observableVentas = this.ventaService.buscarVentaPorVendedorYOperador(this.filtroVendedorUuid, this.filtroOperadorUuid);
    } else if (this.filtroVendedorUuid) {
      observableVentas = this.ventaService.buscarVentaPorVendedor(this.filtroVendedorUuid);
    } else if (this.filtroOperadorUuid) {
      observableVentas = this.ventaService.buscarVentaPorOperador(this.filtroOperadorUuid);
    } else {
      observableVentas = this.ventaService.obtenerVentas();
    }
  
    observableVentas.subscribe({
      next: (data) => {
        this.ventas = data;
        this.cantidadVentas = this.ventas.length;
        this.calcularTotalVentas();
      },
      error: (error) => console.error('Error al obtener ventas', error)
    });
  }
  calcularTotalVentas() {
    if (Array.isArray(this.ventas) && this.ventas.length > 0) {
      this.totalVentas = this.ventas.reduce((acumulador, ventaActual) => acumulador + (ventaActual.total || 0), 0);
    } else {
      this.totalVentas = 0;
    }
  }

  obtenerVentaPorUuid(uuid: string) {
    this.ventaService.obtenerVentaPorUuid(uuid).subscribe({
      next: (data) => this.recargaSeleccionada = data,
      error: (error) => console.error('Error al obtener cliente', error)
    });
  }

  registrarVenta(ventaData: Venta) {
    this.ventaService.registrarVenta(ventaData).subscribe({
      next: (response) => this.mostrarMensaje('Venta registrado con éxito.', 'success'),
      error: (error) => this.mostrarMensaje('Error al registrar el venta.', 'danger')
    });
  }

  cambiarEstadoDeVenta(clienteUuid: string, estado: string) {
    this.ventaService.cambiarEstadoVenta(clienteUuid, estado).subscribe({
      next: (response) => {
        this.mostrarMensaje('Estado de la venta cambiado con éxito.', 'success');
      },
      error: (error) => {
        this.mostrarMensaje('Error al cambiar el estado de la venta.', 'danger');
      }
    });
  }

  mostrarMensaje(mensaje: string, tipo: string) {
    this.mensajeAlerta = mensaje;
    this.mostrarAlerta = true;

    setTimeout(() => {
      this.mostrarAlerta = false;
    }, 3000);
  }

  irACrearVenta() {
    this.router.navigate(['/ventas/crear-venta']); // Asegúrate de que la ruta esté definida en tu módulo de rutas
  }
}
